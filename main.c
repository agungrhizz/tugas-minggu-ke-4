#include <stdio.h>
#include <stdlib.h>

// Deklarasi konstanta
#define UNIVERSITAS "Gunadarma"

//Fungsi dengan parameter
int mahasiswa(char nama[50], int npm, float ipk){
    printf("Nama: %s\nNPM: %d\nIPK: %.2f\nUniversitas %s\n",nama,npm,ipk,UNIVERSITAS);
    // printf("\nNPM: %d\nNama: %s\nIPK: %f\n",npm,nama,ipk,);
}

//Fungsi dengan nilai balik void atau disebut juga prosedur
void data(){
   mahasiswa("Agung Rizki Setiawan", 17119134, 3.49);
}

int main()
{
    data();
    return 0;
}


